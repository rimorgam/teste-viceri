# Projeto Integração com GitLab

O projeto consiste em criar/importar projetos do gitlab, integração de Issues criadas no GitLab via WebHook.

**Considerar o uso dos seguintes itens:**

- .NET.
  1. Será fornecido dois projetos como acelerador, um em .NET Core 2.0 e o outro em .NET Framework 4.6, uso é opcional.
- SQL Server Express.
- Angular, Angular JS, VUE ou qualquer outro SPA.
  1. Será fornecido um projeto em Angular como acelerador, uso é opcional.
- Testes de unidades.
- SOLID

**Importação - OBRIGATÓRIO** - Est. Aprox. 4 Horas

  1. Importar projetos do GitLab(api) para a plataforma.
    1. Adicionar Flag para sinalizar que o projeto é importado
    2. Adicionar coluna com Id do projeto no GitLab
  2. Validação
    1. Não permitir importar projetos duplicados
    2. Não permitir importar projetos criados via fork

**Integrar via Webhook para notificação de ISSUES - OPCIONAL**  - Est. Aprox. 4 Horas

- Criar endpoint para receber notificações quando uma issue for criada/atualizada
  1. Incluir/Atualizar issue no banco com os seguintes dados
    1. Titulo
    2. Id da Issue no GitLab
    3. Descrição
    4. Data de criação
    5. Estado
  2. Validações
    1. Não poderá persistir Issue com mesmo Id
    2. Caso Issue exista no banco, os dados deverão ser atualizados.

**Documentação/links gitlab**

- [https://gitlab.com/](https://gitlab.com/)
- [https://docs.gitlab.com/ee/README.html](https://docs.gitlab.com/ee/README.html)

